import json

import os
import argparse
import sys
sys.path.append("DL1_framework/Training")
import train_tools as tt
import numpy as np
import h5py
import keras


def GetParser():
    """Argparse option for GRID hyperparameter scan script."""
    parser = argparse.ArgumentParser(description=""" Options for the hyper
                                     parameter optimisation""")

    parser.add_argument('--configs', type=str)
    parser.add_argument('--trainingfile', type=str)
    parser.add_argument('--validationfile', type=str)
    parser.add_argument('--validation_config', type=str)
    parser.add_argument('--variables', type=str)
    parser.add_argument('--outputfile', type=str)
    parser.add_argument('--large_file', action='store_true')
    parser.add_argument('--epochs', type=int, default=130)

    split = parser.add_mutually_exclusive_group()
    split.add_argument('--use_gpu', action='store_true', help='''Optionto use
                       GPUs.''')

    args = parser.parse_args()
    return args


os.environ['KERAS_BACKEND'] = 'tensorflow'

args = GetParser()
use_gpu = args.use_gpu
# import tensorflow as tf
# from keras.backend.tensorflow_backend import set_session

# dc = {'GPU': 1 if use_gpu else 0, 'CPU': 1}
# print('dev', dc)

# config = tf.ConfigProto(device_count=dc)
# config.gpu_options.per_process_gpu_memory_fraction = 0.95
# set_session(tf.Session(config=config))

from keras.layers import BatchNormalization
from keras.layers import Dense, Activation, Input
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import ReduceLROnPlateau, Callback


def GetRejection(y_pred, y_true):
    """Calculates the c and light rejection for 77% WP and 0.018 c-fraction."""
    b_index, c_index, u_index = 2, 1, 0
    cfrac = 0.018
    target_beff = 0.77
    y_true = np.argmax(y_true, axis=1)
    b_jets = y_pred[y_true == b_index]
    c_jets = y_pred[y_true == c_index]
    u_jets = y_pred[y_true == u_index]
    bscores = np.log(b_jets[:, b_index] / (cfrac * b_jets[:, c_index] +
                                           (1 - cfrac) * b_jets[:, u_index]))
    cutvalue = np.percentile(bscores, 100.0 * (1.0 - target_beff))

    c_eff = len(c_jets[np.log(c_jets[:, b_index] / (cfrac * c_jets[:, c_index]
                                                    + (1 - cfrac) *
                                                    c_jets[:, u_index])) >
                       cutvalue]) / float(len(c_jets))
    u_eff = len(u_jets[np.log(u_jets[:, b_index] / (cfrac *
                                                    u_jets[:, c_index] +
                                                    (1 - cfrac) *
                                                    u_jets[:, u_index])) >
                       cutvalue]) / float(len(u_jets))

    return 1. / c_eff, 1. / u_eff


class MyCallback(Callback):
    def __init__(self, X_valid, Y_valid):
        self.result = {}
        self.dict_list = []
        self.X_valid = X_valid
        self.Y_valid = Y_valid
        self.acc_key = 'accuracy'
        if keras.__version__ <= '2.2.4':
            self.acc_key ='acc'

    def on_epoch_end(self, epoch, logs=None):
        y_pred = self.model.predict(self.X_valid, batch_size=5000)
        c_rej, u_rej = GetRejection(y_pred, self.Y_valid)
        dict_epoch = {
            "epoch": epoch,
            "loss": float(logs['loss']),
            "acc": float(logs[self.acc_key]),
            "val_loss": float(logs['val_loss']),
            "val_acc": float(logs[f'val_{self.acc_key}']),
            "c_rej": c_rej,
            "u_rej": u_rej
        }
        self.dict_list.append(dict_epoch)

    def on_train_end(self, logs=None):
        min_val_loss = 99
        for i, elem in enumerate(self.dict_list):
            if elem['val_loss'] < min_val_loss:
                min_val_loss = elem['val_loss']
                self.result = elem
        self.result = {"best": self.result, "all": self.dict_list}


def data_loader(trainingfile, validationfile, validation_config,
                variables):
    """loads the data to memory."""
    X_valid, Y_valid = tt.Get_TestSamples(validationfile,
                                          validation_config,
                                          variables, origin=True)

    if args.large_file:
        return None, None, X_valid, Y_valid

    X_train, Y_train = tt.Get_TrainingSample(trainingfile,
                                             variable_config=variables,
                                             do_weights=False)

    return X_train, Y_train, X_valid, Y_valid


def NN_train(config, X_train, Y_train, X_valid, Y_valid):
    inputs = Input(shape=(X_valid.shape[1],))
    x = inputs
    for i, unit in enumerate(config["units"]):
        x = Dense(units=unit, activation="linear",
                  kernel_initializer='glorot_uniform')(x)
        x = BatchNormalization()(x)
        x = Activation(config["activations"][i])(x)
    predictions = Dense(units=3, activation='softmax',
                        kernel_initializer='glorot_uniform')(x)

    model = Model(inputs=inputs, outputs=predictions)
    # model.summary()

    model_optimizer = Adam(lr=config["lr"])
    model.compile(  # loss='mse',
        loss='categorical_crossentropy',
        optimizer=model_optimizer,
        metrics=['accuracy'])
    reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.2,
                                  patience=5, min_lr=0.00001)
    my_callback = MyCallback(X_valid, Y_valid)

    callbacks = [reduce_lr, my_callback]

    if args.large_file:
        file = h5py.File(args.trainingfile, 'r')
        X_train = file['X_train']
        Y_train = file['Y_train']
        params = {'batch_size': int(config["batch_size"]),
                  'X': X_train,
                  'Y': Y_train
                  }

        training_generator = tt.DataGenerator_ReadFile(**params)
        model.fit_generator(generator=training_generator,
                            validation_data=[X_valid, Y_valid],
                            epochs=args.epochs, callbacks=callbacks,
                            use_multiprocessing=True)
    else:
        model.fit(X_train, Y_train,
                  validation_data=[X_valid, Y_valid],
                  batch_size=int(config["batch_size"]), epochs=args.epochs,
                  callbacks=callbacks
                  )

    del model
    return my_callback.result


def train_config(hash, result):

    return {'performances': result,
            'config': hash}


def train():
    print("load data")
    X_train, Y_train, X_valid, Y_valid = data_loader(args.trainingfile,
                                                     args.validationfile,
                                                     args.validation_config,
                                                     args.variables)
    print("Read in config files")
    configs = []
    for f in args.configs.split(','):
        with open('%s' % f) as config:
            configs += json.load(config)

    print("Start HP training")
    results = []
    for i, config in enumerate(configs):
        if i % 10 == 0:
            print("Training", i, "/", len(configs))
        result = NN_train(config, X_train, Y_train, X_valid, Y_valid)
        result = train_config(config['hash'], result)
        results.append(result)
        with open(args.outputfile, 'w') as outfile:
            json.dump(results, outfile, indent=4)


if __name__ == '__main__':
    train()
