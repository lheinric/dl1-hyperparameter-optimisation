# Hyperparameter Optimisation for DL1

This HP optimisation is desgined to run on the grid.

The main script is ```train.py``` with the following options
```
usage: train.py [-h] [--configs CONFIGS] [--trainingfile TRAININGFILE]
                [--validationfile VALIDATIONFILE]
                [--validation_config VALIDATION_CONFIG]
                [--variables VARIABLES] [--outputfile OUTPUTFILE]
```
* configs are a bunch of json files, in which the different HP are defined. They have to be passed like ```--configs config_1.json config_2.json config_3.json``` or with the bash synthax e.g. ```folder/*.json```.
* trainingfile: path to h5 file for training
* validationfile: path to h5 file for testing
* validation_config: json file with scaling, shifting and default values for the input variables
* variables: json file with list of variables which are used for training
* outputfile: defines the json outputfile containing the training and validation performance of the models

## Setup
Clone this repository via
```
git clone --recursive https://gitlab.cern.ch/aml/tutorials/dl1-hyperparameter-optimisation.git
```

you can use the following docker image `gitlab-registry.cern.ch/aml/tutorials/dl1-hyperparameter-optimisation:latest` to test the code locally.

Alternatively when working on a `/cvmfs` enabled machine you can also use this image which should be faster to load `singularity exec -B /eos '/cvmfs/unpacked.cern.ch/registry.hub.docker.com/atlasml/ml-base:latest' bash`

To submit the GRID jobs you need to do the setup in an ATLAS environment
```
setupATLAS
lsetup rucio
lsetup panda
```
if you don't have a valid proxy you need to run also `voms-proxy-init -voms atlas`

## Generation of Config Files

In order to generate the config files you can use the script ```Config_Generator.py```
```
usage: Config_Generator.py [-h] [--dicts_per_file DICT_PER_FILE]
                           [--config CONFIG] [--output_base OUTPUT_BASE]
```
With the option ```--config``` one passes a json file with the following synthax
```
{
   "lr": [0.1, 0.01, 20],
   "units": [[256, 512], [256, 512], 24, 12, 6],
   "activations": ["relu", "linear"],
   "batch_size": [100, 3000, 30]
 }
```
 for the learning rate there are 2 supported synthaxes:
```
"lr": [0.1, 0.01, 20]
```
will use the function numpy.linspace(*[0.1, 0.01, 20]) to generate different learning rates and
```
"lr": [[0.1, 0.01]]
```
will take the inner list as different learning rates. The same applies for ```batch_size```.

The units define the number of hidden layers as well as the used units within the hidden layer. It is possible to pass a flat list which fully defines the amount and units of Dense layers.
Passing a list within the list then varies the units using the entries of the inner list, e.g.
```
"units": [[30, 36, 42],24, 12, 6]
```
means the architecture has 4 hidden Dense layers and for the first layer the units are either 30, 36 or 42 (they will be permutaded).

In the case of the activation functions, one can either give a single string, a flat list e.g.
```
"activations": ["relu", "linear"]
```
which will result in a simoultaneous usage of the activation function in each layer but varied for different HP options.
The second option is then a list within the list
```
"activations": [["relu", ["linear", "relu"], "relu","relu"]]
```
which will assign each layer the specified activation function. NOTE: the length of hidden layers and activation functions has to match in this case.

## Testing locally

You can test if the script is working locally. If you're working on lxplus you can set up a working environment using singularity:

```
singularity exec -B /eos '/cvmfs/unpacked.cern.ch/registry.hub.docker.com/atlasml/ml-base:latest' bash
```

Then you can test a training:

```
python train.py --configs configs/HP_config_*.json --validationfile /eos/user/m/mguth/public/btagging-ml_tutorial_files/MC16d_hybrid_odd_100_PFlow-validation.h5 --trainingfile /eos/user/m/mguth/public/btagging-ml_tutorial_files/MC16d_hybrid-training_sample-NN.h5 --variables DL1_framework/Training/configs/DL1r_Variables.json --validation_config DL1_framework/Preprocessing/dicts/params_MC16D-ext_2018-PFlow_70-8M_mu.json --large_file --epochs 1 --outputfile output.json
```
before you need to generate your config files
```
python Config_Generator.py --dicts_per_file 3 --config HP_config-generate.json --output_base HP_config
```

**Note:** that this will run over the all the files matching `configs/HP_config_*.json`, you should have generated this in the last step but you may want to restrict this.

## Running on the GRID
First you need to produce the config files with the hyper parameters, e.g. via
```
python Config_Generator.py --dicts_per_file 3 --config HP_config-generate.json --output_base HP_config
```

They will appear in the folder `configs`. 

As next step you need to uplaod the necessary files to rucio. You need 2 different data sets. One containing all the configuration files and one with all the necessary files for training.
There are 2 example files for the rucio upload ([`up_rucio.sh`](https://gitlab.cern.ch/aml/tutorials/dl1-hyperparameter-optimisation/blob/master/up_rucio.sh) for all supporting files and [`rucio_up.sh`](https://gitlab.cern.ch/aml/tutorials/dl1-hyperparameter-optimisation/blob/master/configs/rucio_up.sh) for the config files). You need to adapt the files according to your needs.


The submission command looks as the following
```
prun --containerImage \
docker://gitlab-registry.cern.ch/aml/tutorials/dl1-hyperparameter-optimisation:latest \
--exec 'PYTHONPATH=/btagging/DL1_framework/Training:$PYTHONPATH \
python /btagging/train.py --configs %IN \
--validationfile /ctrdata/hp_scan.btagtutorial.DL1r_test_file \
--trainingfile /ctrdata/hp_scan.btagtutorial.DL1r_train_file \
--variables /ctrdata/hp_scan.btagtutorial.DL1r_varfile \
--validation_config /ctrdata/hp_scan.btagtutorial.DL1r_validconf \
--outputfile out.json \
--epochs 1 --large_file' \
--inDS user.mguth:user.mguth.dl1.hp.optimisation.configs_test \
--secondaryDS IN2#4#user.mguth.dl1r.files_hpscan \
--outDS user.${RUCIO_ACCOUNT}.hp.test.$(date +%Y%m%d%H%M%S) \
--forceStaged \
--noBuild \
--forceStagedSecondary \
--reusableSecondary=IN2 \
--outputs out.json \
--nFilesPerJob 3 \
--disableAutoRetry \
--cmtConfig nvidia-gpu \
--tmpDir /tmp \
--site ANALY_MANC_GPU_TEST
```
